/* Описати своїми словами навіщо потрібні функції у програмуванні.
Для того щоб мати змогу зручно викликати в різних частинах коду однакову,
або схожу, послідовність команд. При цьому не повторювати код та, фактично,
зменшити його об'єм.

Описати своїми словами, навіщо у функцію передавати аргумент.
Аргумент необхідний для того щоб передати функції певне значення,
з яким вона буде працювати відповідно до заданих умов.
Якщо аргумент не переданиЙ, функція не зможе виконати необхідну задачу.

Що таке оператор return та як він працює всередині функції?
"return" повертає значення функції в частину коду, яка її викликала.
Функція завжди щось повертає. Якщо "return" не вказано, або не зазначено
що саме функція повинна повернути, функція повертає "undefined".
"return" зазначається в будь-якому місці функції. Коли функція доходить
до нього, вона зупиняється і повертає значення в код.
      */

let numFirst = +prompt(`Enter the First Number`);
let numSecond = +prompt(`Enter the Second Number`);
let operator = prompt(`Enter one of the operators: +, -, *, /`);

while (isNaN(numFirst) || isNaN(numSecond) || numFirst == '' || numSecond == '') {
  numFirst = +prompt(`Enter the First Number`, numFirst);
  numSecond = +prompt(`Enter the Second Number`, numSecond);
}

function calcResult(numFirst, numSecond, operator) {
  switch (operator) {
    case `+`:
      return numFirst + numSecond;
    case `-`:
      return numFirst - numSecond;
    case `*`:
      return numFirst * numSecond;
    case `/`:
      return numFirst / numSecond;
  }
}

console.log(calcResult(numFirst, numSecond, operator));

